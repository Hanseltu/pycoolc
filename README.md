OVERVIEW
==
-----
**work in progress**

A compiler for COOL (Classroom Object Oriented Language) written in Python.

COOL Introduction
==
Cool,an acronym for Classroom Object Oriented Language,is a computer programming language designed
by Alexander Aiken for use in an undergraduate compiler course project.While small enough for a one
term project,Cool still has many of the feature of modern programming languages,including objects,
automatic memory management,strong typing and simple reflection.The reference Cool compiler is written
in C++,built fully on the public domain tools.It generates code for a MIPS simulator,SPIM.Thus,the
languages should port easily to other platforms.
